<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = [
        'store_id', 'user_id', 'product_id', 'ordered_at', 'donation_expiry_date'
    ];  

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function products()
    {
        return $this->hasMany(DonationProduct::class, 'donation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
