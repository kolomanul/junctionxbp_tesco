<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationProduct extends Model
{
    protected $fillable = [
        'donation_id', 'product_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'id');
    }

    public function donation()
    {
        return $this->belongsTo(Donation::class, 'donation_id');
    }
}
