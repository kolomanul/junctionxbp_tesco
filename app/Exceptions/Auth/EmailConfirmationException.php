<?php

namespace App\Exceptions\Auth;

use Exception;
use App\Exceptions\BaseException;

class EmailConfirmationException extends BaseException
{
    protected $message = "Email confirmation could not be sent";
    protected $statusCode = 500;
}
