<?php

namespace App\Exceptions\Auth;

use Exception;
use App\Exceptions\BaseException;

class ForgotPasswordException extends BaseException
{
    protected $message;
    protected $statusCode = 401;

    public function __construct($message)
    {
        $this->message = $message;
    }
}
