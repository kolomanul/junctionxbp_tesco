<?php

namespace App\Exceptions\Auth;

use Exception;
use App\Exceptions\BaseException;

class InvalidCredentialsException extends BaseException
{
    protected $message = "Invalid credentials.";
    protected $statusCode = 404;
}
