<?php

namespace App\Exceptions\Auth;

use Exception;
use App\Exceptions\BaseException;

class NotExistException extends BaseException
{
    protected $message = "User does not exist.";
    protected $statusCode = 404;
}
