<?php

namespace App\Exceptions;

use Exception;

class BadCredentials extends InvalidRequestData
{
    protected $message = "Invalid credentials";
    protected $statusCode = 404;
}
