<?php

namespace App\Exceptions;

use Exception;

class BaseException extends Exception
{

    private $statusCode  = 404;
    protected $errorCode = 0;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respondNotFound($message = 'Not found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondNotAllowed($message = 'Method not allowed')
    {
        return $this->setStatusCode(405)->respondWithError($message);
    }

    public function respondRequirementNotMet($message = 'Requirement not met')
    {
        return $this->setStatusCode(460)->respondWithError($message);
    }

    public function respondInvalidRequestData($message = 'Invalid request data')
    {
        return $this->setStatusCode(422)->respondWithError($message);
    }

    public function respondBadRequest($message = 'Bad request')
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    public function respondServerError($message = 'Server error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode(),
                'error_code' => $this->errorCode
            ]
        ]);

    }

    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }
}
