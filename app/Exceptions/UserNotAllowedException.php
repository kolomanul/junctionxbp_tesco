<?php

namespace App\Exceptions;

use Exception;

class UserNotAllowedException extends BadRequest
{
    protected $message = "User not allowed to perform such action.";
    protected $statusCode = 405;
}
