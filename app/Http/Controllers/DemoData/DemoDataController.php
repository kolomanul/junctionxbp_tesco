<?php

namespace App\Http\Controllers\DemoData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Store;

class DemoDataController extends Controller
{
    public function makeData()
    {
        $store = Store::create([
            'name'              =>  'Tesco Budapest',
            'tesco_store_id'    => 'rand0m_id_go3s_her3'
        ]);

        $products = factory(Product::class, 10)->make();

        foreach ($products as $product) {
            $data = $product->toArray();
            $data['store_id'] = $store->id;
            Product::create($data);
        }

    }
}
