<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required', 
            'tesco_store_id' => 'string|required',
            'category' => 'string', 
            'best_before' => 'date', 
            'use_by' => 'date', 
            'other_data' => 'text', 
        ];
    }
}
