<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'tesco_store_id', 'name', 'latitude', 'longitude'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
