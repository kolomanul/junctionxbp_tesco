<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedSmallInteger('role')->default(User::ROLE_NON_WORKER);
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            'name'      => 'Kulcsar Kalman',
            'email'     => 'kulcsar_kalman@yahoo.com',
            'password'  => Hash::make('123qwe'),
            'role'      => User::ROLE_ADMIN
        ]);

        User::create([
            'name'      => 'Nagy Lilla',
            'email'     => 'n.lilla@yahoo.com',
            'password'  => Hash::make('123qwe'),
            'role'      => User::ROLE_ADMIN
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
