<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('donation_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('donation_products', function (Blueprint $table) {
            $table->foreign('donation_id')->references('id')->on('donations')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donation_products', function (Blueprint $table) {
            if (Schema::hasColumn('donation_products', 'store_id'))
            {
                Schema::table('donation_products', function (Blueprint $table) {
                    $table->dropForeign('donation_products_store_id_foreign');
                    $table->dropForeign('donation_products_user_id_foreign');
                });
            }
        });
        Schema::dropIfExists('donation_products');
    }
}
