<?php

use Illuminate\Database\Seeder;

class PassportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_personal_access_clients')->insert([
            'id' => 1,
            'client_id' => 1,
            'created_at' => '2018-04-28 00:00:00',
            'updated_at' => '2018-04-28 00:00:00',
        ]);

        DB::table('oauth_clients')->insert([
            'id' => 1,
            'name' => 'Laravel Personal Access Client',
            'secret' => 'H8SCN7yPBXPnlHagS7wN9pBXi3GwU3hIFuZDeWSm',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
            'created_at' => '2018-04-28 00:00:00',
            'updated_at' => '2018-04-28 00:00:00',
        ]);

        DB::table('oauth_clients')->insert([
            'id' => 2,
            'name' => 'Laravel Password Grant Client',
            'secret' => 'jA5lTmJu8lJGZsg4PaWE6QlCPIM8pP3QlIBnYat4',
            'redirect' => 'http://localhost',
            'personal_access_client' => '0',
            'password_client' => 1,
            'revoked' => 0,
            'created_at' => '2018-04-28 00:00:00',
            'updated_at' => '2018-04-28 00:00:00',
        ]);
    }
}
