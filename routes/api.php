<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Auth'], function() {
    Route::post('oauth/token', 'AuthController@issueToken');
    Route::post('register', 'AuthController@register');
});


Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['namespace' => 'User'], function() {
        Route::apiResource('user', 'UserController');
    });
    Route::group(['namespace' => 'Store'], function() {
        Route::apiResource('store', 'StoreController');
    });
    Route::group(['namespace' => 'Product'], function() {
        Route::apiResource('product', 'ProductController');
    });
    Route::group(['namespace' => 'Donation'], function() {
        Route::apiResource('donation', 'DonationController');
    });
    Route::group(['namespace' => 'DemoData'], function() {
        Route::post('makeData', 'DemoDataController@makeData');
    });

});
